# esp32galileo

ESP32 firmware providing a REST API for controlling the galileo science outreach setup

@aideAPI

## A ESP32 micro-controller to drive the galileo board experiment

This software package provides the [ESP32](https://en.wikipedia.org/wiki/ESP32) micro-controller firmware to drive the galileo board experiment.

![Setup view](https://line.gitlabpages.inria.fr/aide-group/esp32galileo/view.png "Setup view")

### Web interface

![HTML web interface](https://line.gitlabpages.inria.fr/aide-group/esp32galileo/setup_galileo_page.png "HTML web interface")

### Doing the experiment

- Install the four optical sensors either
  - at equal distances in order to see the acceleration, or
  - at increasing distances to obtain equal time intervals.
- Stick the metal ball onto the electromagnet.
- Press the button on the setup of the HTML interface `Run` button and observe the ball fall.
- Press the HTML interface `Get` button at the motion end to get the detected times, velocities and accelerations.

Tutorial video: [https://youtu.be/ew7ONP2M09M](https://youtu.be/ew7ONP2M09M)

### Remarks:

- The `Start` and `Stop` command are only used for debug.
- The `Get` output is of the form
```
{
  "route": 
  "galileo", 
  "time": 0.000,        // Current time in second when the `Get` button is pressed.
  "start_time": 0.000,  // Experiment time in second, when the `Run` button is pressed.
  "detected_times": [0.000, 0.000, 0.000, 0.000], // Relative times of optical sensor detection, after start_time.
  "detected_velocities": [0.000, 0.000, 0.000],   // Times differences of optical sensor detection.
  "detected_accelerations": [0.00000, 0.00000]    // Times differences of times differences.
}
```
- Result prediction:
  - The ball trajectory writes `x(t) = k t^2`, since the initial velocity is zero, and considering the electromagnet position as origin
    - with `k = 1/2 g sin(alpha) ~ 1.5` when `g = 9.81 m/s^2` is the gravity constant `alpha ~ 15 deg` is the board inclination.
  - In case of equal distances we expect the detected times and velocities to decrease, and accelerations to be similar.
  - In case of increasing distance we expect the detected times differences to be similar.

## References

- The [science outreach activity and mechanical hardware description](https://pixees.fr/plan-incline-de-galilee/) (in French)
- The [electronic hardware description](https://gitlab.inria.fr/line/aide-group/esp32galileo/-/blob/master/src/hardware.md)
- The [webservice usage description](https://gitlab.inria.fr/line/aide-group/esp32galileo/-/blob/master/src/usage_galileo.md)



<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/esp32galileo'>https://gitlab.inria.fr/line/aide-group/esp32galileo</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/esp32galileo'>https://line.gitlabpages.inria.fr/aide-group/esp32galileo</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/esp32galileo/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/esp32galileo/-/tree/master/src</a>
- Version `1.1.1`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/esp32galileo.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage


- Refer to <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/esp32galileo/-/blob/master/src/usage_galileo.md'>usage_galileo</a>, for the Galileo mechanism web service usage.

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>esp32gpiocontrol: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol'>ESP32 firmware providing a REST API for controlling the GPIO interface and higher functions</a></tt>

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Authors

- Éric Pascual&nbsp; <big><a target='_blank' href='mailto:eric.g.pascual@gmail.com'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://twitter.com/ericpobot'>&#128463;</a></big>
- Martine Olivi&nbsp; <big><a target='_blank' href='mailto:martine.olivi@inria.fr'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://www-sop.inria.fr/members/Martine.Olivi/'>&#128463;</a></big>
- Sabrina Barnabé&nbsp; <big><a target='_blank' href='mailto:snjl-contact@gmail.com'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://snjl.fr'>&#128463;</a></big>
- Thierry Viéville&nbsp; <big><a target='_blank' href='mailto:thierry.vieville@inria.fr'>&#128386;</a></big>
