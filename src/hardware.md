## Electronic elements and wiring

The realization of the electronic harware is relatively straightforward.

### What to buy ?

Beyond the woodwork we have to buy

- 1 [iron ball](https://driveroulement.com/billes-acier/5467-billes-acier-bille-acier-25mm.html)
- 1 [electromagnet](https://www.adafruit.com/product/3872)
- 1 [driver chip](https://www.adafruit.com/product/970)
- 4 [optical sensors](https://www.dfrobot.com/product-85.html)
- 1 [push button](https://www.dfrobot.com/product-1097.html)
- 1 [ESP32 board](https://www.adafruit.com/product/3619)
- 1 [Proto board](https://www.adafruit.com/product/2884)
- 2 [Power supplies](https://www.dfrobot.com/product-1940.html)
- 6 [connectors](https://www.mouser.fr/ProductDetail/Adafruit/2830?qs=%2Fha2pyFadui4%252BQPpXTlz07ZI1m0Dkou9D8%2FCO6HOn17k98fNnUwU7MkKKGRRQtH57ThbCKV3uRl14kOJwM0MyDtTATZJnn%2FSRgNxbUxJDav9fijAFpWNaQ%3D%3D)
- 6 [wire extensions](https://www.mouser.fr/ProductDetail/DFRobot/FIT0297?qs=sGAEpiMZZMtyU1cDF2RqUDZGC8letk%2FMjpJSTV5b2hc%3D)

### How to cable ?

- The driverchip is to be installed on the proto board with all connectors towards the optical sensors and electromagnets.
- One power supply is used for the ESP32 board and the other for all devices (inclduing electromagnets) on the protoboard.

![Wiring view](./wiring-view.png "Wiring view")

![Wiring plan](./wiring.png "Wiring plan")
