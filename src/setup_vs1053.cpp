// Implements the VS1053 sound card MP3 music file playing minimal functionnality

#include "handler.hpp"

//
// vs1053 firmware
//

#include <SPI.h>
#include <Adafruit_VS1053.h>
#include <SD.h>

#define RESET -1   // VS1053 Reset pin (not used)
#define DCS 33     // VS1053 XDCS  Data/command select pin (output)
#define DREQ 15    // VS1053 DREQ  Data request, ideally an Interrupt pin
#define CS 32      // VS1053 MP3CS Chip select pin (output)
#define CARDCS 14  // Card   SDCS  SD Card chip select pin

Adafruit_VS1053_FilePlayer *musicPlayer = NULL;

void vs1053_init()
{
  if(musicPlayer == NULL) {
    // Initializes the file player
    static Adafruit_VS1053_FilePlayer vs1053_fileplayer =
      Adafruit_VS1053_FilePlayer(RESET, CS, DCS, DREQ, CARDCS);
    musicPlayer = &vs1053_fileplayer;
    // Connects to the VS1053
    if(!musicPlayer->begin()) {
      Serial.println("Error: VS1053 not found");
    }
    // Sets volume for left, right channels. lower numbers == louder volume!
    musicPlayer->setVolume(10, 10);
    // Plays a tone to indicate VS1053 is working
    musicPlayer->sineTest(0x44, 500);
    // Connects to the SD CARD
    if(!SD.begin(CARDCS)) {
      Serial.println("Error: SD card not found");
    }
  }
}
void vs1053_playFile(const char *file)
{
  vs1053_init();
  musicPlayer->startPlayingFile(file);
}
void vs1053_playFileSync(const char *file)
{
  vs1053_init();
  musicPlayer->playFullFile(file);
}
/* References:
 *  - https://learn.adafruit.com/adafruit-music-maker-featherwing/pinouts
 *  - https://github.com/danclarke/Adafruit_VS1053_Library/blob/master/examples/feather_player/feather_player.ino
 *  - https://forum.arduino.cc/index.php?topic=624003.0
 */

//
// vs1053 service handlers
//

void handle_vs1053_play()
{
  Serial.println("handle_vs1053_play");
  String file = server->pathArg(0);
  vs1053_playFileSync(file.c_str());
  answer(true, "{\"route\" : \"handle_vs1053_play\" \"file\": \"%s\"}", file.c_str());
}
//
// Service configuration
//

void setup_vs1053_service()
{
  server->on("/vs1053/play/{}", HTTP_POST, handle_vs1053_play);
  // Test
  {
    Serial.println("Test");
    Adafruit_VS1053 vs1053 = Adafruit_VS1053(RESET, CS, DCS, DREQ);
    if(!vs1053.begin()) {
      Serial.println("Error: VS1053 fuck not found");
    }
  }
}
