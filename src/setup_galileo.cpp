// Implements specific functionnalities of a "Galileo board" scientific object

#include "handler.hpp"

//
// Galileo service handlers
//

#define PRESS_BUTTON 34

#define SENSOR_1 27
#define SENSOR_2 25
#define SENSOR_3 12
#define SENSOR_4 13

#define MAGNET_1 26

// Implements the on/off button mechamism

unsigned int handle_galileo_off_last_time = 0;

void handle_galileo_button(bool on_else_off)
{
  verbose(on_else_off ? "handle_galileo_on_off(1)" : "handle_galileo_on_off(0)");
  if(!on_else_off) {
    handle_galileo_off_last_time = millis();
  }
  digitalWrite(MAGNET_1, on_else_off);
}
void handle_galileo_on()
{
  handle_galileo_button(true);
}
void IRAM_ATTR handle_galileo_run()
{
  handle_galileo_button(false);
  setInterval(handle_galileo_on, 1000, 1);
}
void handle_galileo_start()
{
  // Configures the magnet output
  {
    pinMode(MAGNET_1, OUTPUT);
    handle_galileo_on();
  }

  // Registers the on/off button mechamism
  {
    pinMode(PRESS_BUTTON, INPUT);
    attachInterrupt(digitalPinToInterrupt(PRESS_BUTTON), handle_galileo_run, RISING);
    handle_galileo_off_last_time = 0;
  }

  // Registers sensor input timing measure
  {
    gpio_digital_timing_start(SENSOR_1, RISING);
    gpio_digital_timing_start(SENSOR_2, RISING);
    gpio_digital_timing_start(SENSOR_3, RISING);
    gpio_digital_timing_start(SENSOR_4, RISING);
  }
}
void handle_galileo_stop()
{
  detachInterrupt(digitalPinToInterrupt(PRESS_BUTTON));

  gpio_digital_timing_stop(SENSOR_1);
  gpio_digital_timing_stop(SENSOR_2);
  gpio_digital_timing_stop(SENSOR_3);
  gpio_digital_timing_stop(SENSOR_4);

  digitalWrite(MAGNET_1, 0);
}
void hande_galileo_get()
{
  // Converts in seconds and skip negative values
  double d[5] = {
    0.001 * handle_galileo_off_last_time,
    0.001 * ((int) gpio_digital_timing_get(SENSOR_1) - (int) handle_galileo_off_last_time),
    0.001 * ((int) gpio_digital_timing_get(SENSOR_2) - (int) handle_galileo_off_last_time),
    0.001 * ((int) gpio_digital_timing_get(SENSOR_3) - (int) handle_galileo_off_last_time),
    0.001 * ((int) gpio_digital_timing_get(SENSOR_4) - (int) handle_galileo_off_last_time)
  };
  for(unsigned int i = 0; i < 5; i++) {
    d[i] = MAX(0, d[i]);
  }
  //
  answer(true, "{\"route\": \"galileo\", \"time\": %.3f, \"start_time\": %.3f, \"detected_times\": [%.3f, %.3f, %.3f, %.3f], \"detected_time_differences\": [%.3f, %.3f, %.3f], \"detected_time_difference_variations\": [%.5f, %.5f]}", fmillis(), d[0], d[1], d[2], d[3], d[4], d[2] - d[1], d[3] - d[2], d[4] - d[3],
         2 * d[2] - d[1] - d[3], 2 * d[3] - d[2] - d[4]);
}
//
// Service configuration
//

void handle_galileo()
{
  String action = server->arg("action");
  if(action == "start") {
    handle_galileo_start();
  } else if(action == "run") {
    handle_galileo_run();
  } else if(action == "stop") {
    handle_galileo_stop();
  }
  hande_galileo_get();
}
void handle_galileo_page()
{
#include "setup_galileo_page.hpp"
  server->send(200, "text/html", setup_galileo_page);
}
void setup_galileo_service()
{
  server->on("/", HTTP_GET, handle_galileo_page);
  server->on("/galileo", HTTP_POST, handle_galileo);
}
