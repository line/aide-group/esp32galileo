@aideAPI

## A ESP32 micro-controller to drive the galileo board experiment

This software package provides the [ESP32](https://en.wikipedia.org/wiki/ESP32) micro-controller firmware to drive the galileo board experiment.

![Setup view](https://line.gitlabpages.inria.fr/aide-group/esp32galileo/view.png "Setup view")

### Web interface

![HTML web interface](https://line.gitlabpages.inria.fr/aide-group/esp32galileo/setup_galileo_page.png "HTML web interface")

### Doing the experiment

- Install the four optical sensors either
  - at equal distances in order to see the acceleration, or
  - at increasing distances to obtain equal time intervals.
- Stick the metal ball onto the electromagnet.
- Press the button on the setup of the HTML interface `Run` button and observe the ball fall.
- Press the HTML interface `Get` button at the motion end to get the detected times, velocities and accelerations.

Tutorial video: [https://youtu.be/ew7ONP2M09M](https://youtu.be/ew7ONP2M09M)

### Remarks:

- The `Start` and `Stop` command are only used for debug.
- The `Get` output is of the form
```
{
  "route": 
  "galileo", 
  "time": 0.000,        // Current time in second when the `Get` button is pressed.
  "start_time": 0.000,  // Experiment time in second, when the `Run` button is pressed.
  "detected_times": [0.000, 0.000, 0.000, 0.000], // Relative times of optical sensor detection, after start_time.
  "detected_velocities": [0.000, 0.000, 0.000],   // Times differences of optical sensor detection.
  "detected_accelerations": [0.00000, 0.00000]    // Times differences of times differences.
}
```
- Result prediction:
  - The ball trajectory writes `x(t) = k t^2`, since the initial velocity is zero, and considering the electromagnet position as origin
    - with `k = 1/2 g sin(alpha) ~ 1.5` when `g = 9.81 m/s^2` is the gravity constant `alpha ~ 15 deg` is the board inclination.
  - In case of equal distances we expect the detected times and velocities to decrease, and accelerations to be similar.
  - In case of increasing distance we expect the detected times differences to be similar.

## References

- The [science outreach activity and mechanical hardware description](https://pixees.fr/plan-incline-de-galilee/) (in French)
- The [electronic hardware description](https://gitlab.inria.fr/line/aide-group/esp32galileo/-/blob/master/src/hardware.md)
- The [webservice usage description](https://gitlab.inria.fr/line/aide-group/esp32galileo/-/blob/master/src/usage_galileo.md)


