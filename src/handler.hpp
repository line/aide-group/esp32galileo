// Defines general functions and variables to be used by specific mechanisms

// If 1 messages are printed on the serial link for debugging, else 0
#define VERBOSE 1

// Web server functionalities

#include <WebServer.h>

extern WebServer *server;

// Answering mechanism

#define answer(ok, fmt, ...) sprintf(printf_buffer, fmt, __VA_ARGS__); server->sendHeader("Access-Control-Allow-Origin", "*"); server->sendHeader("Access-Control-Allow-Methods", "GET, PUT, POST"); server->send(ok ? 200 : 400, "application/json", printf_buffer); verbose(printf_buffer);

extern char printf_buffer[1024], mac_address[20], firmware_version[32];

// Useful macros

#include "string.h"

#if VERBOSE
#define verbose(what) Serial.println(what)
#else
#define verbose(what)
#endif

#define MAX(x, y) (((x) > (y)) ? (x) : (y))

#define fmillis() (0.001 * millis())

// ESP32 digital timing definitions

#define MAX_GPIO_DIGITAL_INPUT 32

typedef void (*handler)(void);

void gpio_digital_timing_start(unsigned int index, unsigned int mode);

void gpio_digital_timing_stop(unsigned int index);

unsigned int gpio_digital_timing_get(unsigned int index);

void setInterval(handler call, unsigned int delay = 0, unsigned int count = -1);
