# Installing a the EPS32 galileo mechanism

## Web page

- `http://$IP:80/` opens the user web page

## End user usage

- `curl -X POST -d "action=(start|stop|get)" http://$IP:80/galileo` => `{ "action": \"$action\", "times": [$time_1, $time_2, $time_3, $time_4]}`
  - `start` : Starts the galileo driver
  - `run` : Runs the galileo demo
  - `stop` : Stops the galileo driver
  - `get` : Gets the last galileo data
